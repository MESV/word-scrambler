package org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;
import org.academiadecodigo.tailormoons.wordscrambler.server.commands.CommandHandler;

public class HelpHandler implements ScrambleCommandsHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;
    private Scrambler scrambler;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message, Scrambler scrambler) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;
        this.scrambler = scrambler;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/help")) {
            if (words.length == 1) {
                server.alert("The available commands are:\n" +
                        "/scramble <word>\n" +
                        "/guess <guess>\n" + "/status\n" + "/name <name>\n" + "/logout\n" + "/list\n", clientHandler);

            } else {
                server.alert("Use '/help' to get a list of all available commands.", clientHandler);
            }
        }
    }
}
