package org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands;

import org.academiadecodigo.tailormoons.wordscrambler.client.Client;
import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Timer;
import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class GuessHandler implements ScrambleCommandsHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;
    private Scrambler scrambler;
    private int points = 10;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message, Scrambler scrambler) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;
        this.scrambler = scrambler;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/guess") && clientHandler != server.getScrambler().getCurrentPlayer()) {

            if (server.getScrambler().getWordToScramble() == null) {
                server.alert("You have to start a game before guessing. Try '/scramble <word>'.", clientHandler);
            } else if (words.length == 2) {
                String wordToGuess = words[1].toLowerCase();

                while (!server.getTimer().timeUp()) {

                    if (wordToGuess.equals(server.getScrambler().getWordToScramble())) {
                        server.broadcast("\n" + (clientHandler.getName() + " guessed the scrambled word, which was: "
                                + server.getScrambler().getWordToScramble() + "!\n"), clientHandler);
                        score();
                        server.alert("You've guessed scrambled word and won this round! You have now "
                                + clientHandler.getScore() + " points!", clientHandler);
                        server.getScrambler().setWordToScrambleToNull();

                        //checkWinner();
                        break;

                    } else if (!wordToGuess.equals(server.getScrambler().getWordToScramble())) {

                        if (server.getTimer().halfTime()) {
                            server.alert("You didn't guess the scrambled word, please try again! You have less than "
                                    + (server.getTimer().getSeconds() / 2) + " seconds now. Hurry up!!!", clientHandler);
                            break;
                        } else if (!server.getTimer().halfTime()) {
                            server.alert("You didn't guess the scrambled word, please try again!", clientHandler);
                            break;
                        }
                    }
                }

                if (server.getTimer().timeUp()) {
                    server.alert("The time is up. You can´t try to guess the word now.", clientHandler);
                    server.getScrambler().setWordToScrambleToNull();

                    //checkWinner();
                }
            } else if (words.length > 2) {
                server.alert("You can only enter one word to guess.", clientHandler);

            } else {
                server.alert("Write the desired word after the command: '/guess <word>'.", clientHandler);
            }
        } else {
            server.alert("You cannot guess on your own scrambled word!", clientHandler);
        }
    }

    private void score() {
        server.getScrambler().getCurrentPlayer().incrementScore(points / 2);
        clientHandler.incrementScore(points);
    }

    /*private void checkWinner() {

        if (scrambler.getPlayers().size() == server.getList().size()) {
            server.broadcast("------------------------------------------------", clientHandler);
            server.alert("------------------------------------------------", clientHandler);
            server.broadcast("END OF ROUND " + scrambler.getNumberOfRounds() + ".", clientHandler);
            server.alert("END OF ROUND " + scrambler.getNumberOfRounds() + ".", clientHandler);


            for (ClientHandler player : scrambler.getPlayers()) {
                int score = player.getScore();
                if (scrambler.getPlayers().contains(player)) {
                    server.broadcast(player.getName() + " has now " + player.getScore() + " points.", clientHandler);
                    server.alert(player.getName() + " has now " + player.getScore() + " points.", clientHandler);
                }

            }
            server.broadcast("------------------------------------------------", clientHandler);
            server.alert("------------------------------------------------", clientHandler);

            int score = 0;
            ClientHandler player = null;
            for (int i = 0; i < scrambler.getPlayers().size(); i++) {
                if (score < scrambler.getPlayers().get(i).getScore()) {
                    score = scrambler.getPlayers().get(i).getScore();
                    player = scrambler.getPlayers().get(i);
                }
            }
            server.alert("YOU WIN!!!!", player);
            server.broadcast("Winner: " + player.getName(), player);

        }
    }*/

}