package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class ChangeNameHandler implements CommandHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/name")) {
            if (words.length == 2) {
                String name = words[1];
                server.broadcast((clientHandler.getName() + " just changed his/her name to " + name + "."), clientHandler);
                server.alert("You've just changed your name to " + name + ".", clientHandler);
                clientHandler.setName(name);
                //check availability
            } else if (words.length > 2) {
                server.alert("Your name cannot have spaces.", clientHandler);

            } else {
                server.alert("You have to write the desired name after the command: '/name MyName'.", clientHandler);
            }
        }
    }
}