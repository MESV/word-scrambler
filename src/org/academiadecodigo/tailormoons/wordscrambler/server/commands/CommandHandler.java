package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public interface CommandHandler {

    void execute(ClientHandler clientHandler, Server server, String message);

}
