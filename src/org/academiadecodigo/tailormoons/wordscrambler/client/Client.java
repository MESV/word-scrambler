package org.academiadecodigo.tailormoons.wordscrambler.client;

import org.academiadecodigo.tailormoons.wordscrambler.client.ClientListener;
import org.academiadecodigo.tailormoons.wordscrambler.server.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private int port = 8080;
    private String hostName = "localhost";
    private static Scanner inputScanner;
    private Socket clientSocket;


    public void start() throws IOException {
        clientSocket = new Socket(hostName, port);
        ClientListener clientListener = new ClientListener(clientSocket);
        Thread thread = new Thread(clientListener);
        thread.start();
        Message messages = new Message();
        messages.sendMessages("welcome4");
        inputScanner = new Scanner(System.in);
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

        while (!clientSocket.isClosed()) {
            String message = inputScanner.nextLine();
            out.println(message);
        }
    }
}