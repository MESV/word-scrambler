package org.academiadecodigo.tailormoons.wordscrambler.client;

import org.academiadecodigo.tailormoons.wordscrambler.server.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientListener implements Runnable {

        private final Socket clientSocket;
        private Message messages = new Message();

        public ClientListener(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                while (!clientSocket.isClosed()) {
                    String message = input.readLine();
                    if (message == null) {
                        messages.sendMessages("disconnected1");
                        //System.out.println("Disconnected.");

                        System.exit(1);
                        return;
                    }
                    System.out.println(message);
                }

            } catch (IOException e) {
                System.out.println("Closed Connection.");
            }
        }
    }


